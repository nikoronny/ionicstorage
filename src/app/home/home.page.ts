import { Component } from '@angular/core';
import { StorageService } from '../providers/core-storage/storage.service'
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  public key: string;
  public value: string;
  constructor(private storage: StorageService) { }

  set = () => {
    this.storage.set<string>(this.key, this.value);
  }

  get = () => {
    this.storage.get<string>(this.key).then(response => {
      console.log(response);
    });
  }

  removeAll = () => {
    this.storage.removeAll();
  }

  remove = () => {
    this.storage.remove(this.key);
  }
}
