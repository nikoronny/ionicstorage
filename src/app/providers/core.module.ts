import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StorageModule } from './core-storage/storage.module';



@NgModule({
  declarations: [],
  imports: [
    StorageModule
  ]
})
export class CoreModule { }
