import { NgModule } from '@angular/core';
import { StorageService } from './storage.service';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  imports: [IonicStorageModule.forRoot()],
  providers: [StorageService]
})
export class StorageModule { }
