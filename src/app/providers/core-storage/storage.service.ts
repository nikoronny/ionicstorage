import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }


  public get<T>(key: string): Promise<T> {
    try {
      return this.storage.get(key).then((response: string) => {
        return this.deserializeObject<T>(response);
      });
    } catch {
      return null;
    }
  }

  public set<T>(key: string, value: T): void {
    this.storage.set(key, this.serializeObject(value));
  }

  public remove(key: string): void {
    this.storage.remove(key);
  }

  public removeAll(): void {
    this.storage.clear();
  }

  private serializeObject<T>(object: T) {
    return JSON.stringify(object);
  }

  private deserializeObject<T>(jsonString: string) {
    return JSON.parse(jsonString) as T;
  }
}
